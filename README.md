# CNE Final Project

This project gets you to create a CRUD functional Spring Boot web application that is fully built, tested and deployed by a CI pipeline of your design.

## Contents

- [Help Queue Application](#help-queue-application)
    - [Application Structure](#application-structure)
        - [Frontend Service](#frontend-service)
        - [Gateway Service](#gateway-service)
        - [Backend Service](#backend-service)
        - [Database](#database)
    - [Features](#features)
        - [Basic Functionality](#basic-functionality)
        - [Solutions](#solutions)
        - [Cohorts](#cohorts)
        - [Assignment](#assignment)
        - [Filtering](#filtering)
        - [Topics](#topics)
        - [Keyword Search](#keyword-search)
        - [Urgency](#urgency)
        - [Tagging](#tagging)
- [CI Pipeline](#ci-pipeline)

## Help Queue Application

For this project, you are tasked with creating a Help Queue web application for use in the Academy.

The purpose of this application is to work as a virtual hands-up tool to alert trainers to who needs help in their class.

Trainees can post help tickets to the queue, with oldest tickets at the top of the list. Trainers can then view the queue to see who needs help next.

When a ticket has been solved, it is marked as "Done" by the user and added to a separate "Completed" list. The next-oldest ticket is moved to the top of the list.

Here is a quick mockup of what the UI could look like to give you an idea:

![ui-mockup](https://i.imgur.com/IYtVPyj.png)

At its most basic functionality, the ticket should comprise of:
- Title
- Author
- Description
- Time created

### Application Structure

The application should be built as multiple distinct services. Each of these services should be containerised and deployed as part of a Kubernetes cluster.

#### Frontend Service

Serves the UI for the web application.

The UI is written in React.

All tools in the suite should be accessible via the frontend.

Requests made to the backend services should be made via a "gateway" service, which redirects traffic.

#### Gateway Service

A service dedicated to redirecting requests from the frontend to the various backend APIs for each tool.

This can be a Spring Boot application or a simple NGINX reverse proxy.

#### Backend Service

The backend logic and database querying should be handled by a backend API service.

The MVP requires you to build a single API service that will handle all the backend logic.

This architecture should look like this:

![mvp-structure](images/cne-software-mvp.png)

#### Database

Data should be persisted in a managed AWS RDS database. You will have to create the table schema for this yourself.

Below is the expected entity diagram for a Ticket in the help queue.

![ticket-erd](images/cne-erd.png)

Multiple features in this specification will require you to add more entities to your database and create relationships between them.

### Features

There are multiple stages of development for this application. Each stage increases in complexity.

In addition to basic functionality, you are expected to implement at least 3 features by the end of this project.

#### Basic Functionality

Create basic functionality for the application, including full CRUD.

Users will be able to:
- Create new tickets and add them to the queue
- View tickets in the current queue from oldest to newest
- Delete existing tickets in the queue
- Update existing tickets in the queue

#### Solutions

When a ticket is being marked as "Done", users should be able to add a solution to that ticket, allowing other users to see the solutions to a problem that has already been solved.

#### Cohorts

Implement a help queue per cohort, such that trainees can navigate to their cohort to see the help queue specific to their class.

A cohort page should display the trainers and trainees can make up that cohort.

#### Assignment

Users can assign trainees and trainers to a help ticket. The trainers/trainees they can assign should be dependent on the current cohort.

#### Filtering

Users can filter/arrange tickets based on attributes such as:
- Author
- Date Oldest -> Newest
- Date Newst -> Oldest
- Urgency
- Title (alphabetically)

#### Topics

Tickets should be tagged with a *topic*. This should allow users to filter tickets based on a topic.

#### Keyword Search

Implement a search bar feature, so that users can filter tickets based on keywords.

#### Urgency

Tickets can be marked with different levels of urgency.

#### Tagging

Users can tag themselves to tickets to indicate they are having the same issue.

<details>
<summary>
Stretch Goals - Login Functionality
</summary>

**Implement basic login functionality.**

Users should only be able to:
- Update tickets they have created
- Delete tickets they have created
- Mark tickets they have created as done
- View the help queue for the cohort they are part of

**Implement two levels of privilege: trainer and trainee.**

Trainer privileges:
- Only one who can close tickets
- Can add answers to tickets
- Able to update or delete any trainee's tickets
- Can view any cohort's help queue

Trainee privileges:
- Cannot update or delete other trainee's tickets
- Cannot mark a ticket as "Done"
- Can only view their own cohort's help queue

</details>

## CI Pipeline

Along with developing the software, you are required to create a full CI pipeline that will integrate new code into the working application.

The code should be fully tested, built, containerised and deployed in an EKS cluster automatically when new code is pushed to the source code repository. You are expected to make good use of the feature branch model to achieve this. As this is a group project with multiple developers working on the same codebase, this approach will be imperative.

Creation of the CI pipeline should be handled using Terraform for IaC and Ansible for configuration management. You should be striving for 100% automation.

The infrastructure of your pipeline should follow best practices with regards to security.

It should have the following components:
- Version Control
- Source Code Repository
- CI Server
    - Unit Testing
    - Integration Testing
    - Build Tool
    - Containerisation
- Artefact Repository
- Testing Environment (with test database)
- Infrastructure as Code
- Configuration Management
- Deployment with Elastic Kubernetes Cluster

The below is an example of what such a pipeline may look like. This is not a particularly robust design and should be only be considered a jumping-off point.

![pipeline](https://i.imgur.com/7uuf9ke.png)
